variable "server" {
  description = "Map of project names to configuration."
  type        = map
  default = {
    server01 = {
      server_type = "cx21",
      label       = "first_group"
    }
  }
}
