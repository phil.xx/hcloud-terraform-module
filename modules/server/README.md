## Requirements

| Name | Version |
|------|---------|
| terraform | >= 0.13 |
| hcloud | 1.24.1 |

## Providers

| Name | Version |
|------|---------|
| hcloud | 1.24.1 |

## Modules

No Modules.

## Resources

| Name |
|------|
| [hcloud_server](https://registry.terraform.io/providers/hetznercloud/hcloud/1.24.1/docs/resources/server) |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| dependancy | (Required, string) Name of the server type this server should be created with. | `string` | `"module.network.subnet_id"` | no |
| image | (Required, string) Name or ID of the image the server is created from. | `string` | `"debian-10"` | no |
| label | (Optional, map) User-defined labels (key-value pairs) should be created with. | `string` | `"false"` | no |
| location | (Optional, string) The location name to create the server in. (nbg1, fsn1 or hel1) | `string` | `"fsn1"` | no |
| name | (Required, string) Name of the server to create. | `string` | `"server"` | no |
| network\_id | (int) Unique ID of the network. | `string` | `"module.network.network_id"` | no |
| server\_type | (Required, string) Name of the server type this server should be created with. | `string` | `"cx11"` | no |
| ssh\_keys | (Optional, list) SSH key IDs or names which should be injected into the server at creation time. | `string` | `"pub-key"` | no |

## Outputs

No output.