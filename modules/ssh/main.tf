resource "hcloud_ssh_key" "user" {
  name       = "user"
  public_key = file("~/.ssh/user.pub")
}