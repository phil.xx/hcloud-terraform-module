## Requirements

| Name | Version |
|------|---------|
| terraform | >= 0.13 |

## Providers

| Name | Version |
|------|---------|
| hcloud | n/a |

## Modules

No Modules.

## Resources

| Name |
|------|
| [hcloud_ssh_key](https://registry.terraform.io/providers/hetznercloud/hcloud/latest/docs/resources/ssh_key) |

## Inputs

No input.

## Outputs

| Name | Description |
|------|-------------|
| ssh\_id | The ID of the SSH key |