output "network_id" {
  description = "The ID of the Network"
  value       = hcloud_network.server.id
}

output "subnet_id" {
  description = "The ID of the Subnet"
  value       = hcloud_network_subnet.server.id
}
