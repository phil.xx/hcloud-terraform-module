## Requirements

| Name | Version |
|------|---------|
| terraform | >= 0.13 |
| hcloud | 1.24.1 |

## Providers

| Name | Version |
|------|---------|
| hcloud | 1.24.1 |

## Modules

No Modules.

## Resources

| Name |
|------|
| [hcloud_network_subnet](https://registry.terraform.io/providers/hetznercloud/hcloud/1.24.1/docs/resources/network_subnet) |
| [hcloud_network](https://registry.terraform.io/providers/hetznercloud/hcloud/1.24.1/docs/resources/network) |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| ip\_range | (Required, string) IP Range of the whole Network which must span all included subnets and route destinations. | `string` | `"10.29.0.0/16"` | no |
| name | (Required, string) Name of the Network to create (must be unique per project). | `string` | `"server"` | no |
| subnet\_ip\_range | (Required, string) Range to allocate IPs from. Must be a subnet of the ip\_range of the Network. | `string` | `"10.29.0.0/24"` | no |
| subnet\_network\_zone | (Required, string) Name of network zone. | `string` | `"eu-central"` | no |
| subnet\_type | (Required, string) Type of subnet (server, cloud or vswitch). | `string` | `"cloud"` | no |

## Outputs

| Name | Description |
|------|-------------|
| network\_id | The ID of the Network |
| subnet\_id | The ID of the Subnet |