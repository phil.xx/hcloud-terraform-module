terraform {
  required_version = "<= 0.14"
}

module "ssh" {
  source = "./modules/ssh"
}

module "network" {
  source = "./modules/network"
}

module "server" {
  for_each    = var.server
  source      = "./modules/server"
  ssh_keys    = [module.ssh.ssh_id]
  server_type = each.value.server_type
  name        = each.key
  network_id  = module.network.network_id
  dependancy  = module.network.subnet_id
  label       = each.value.label
}
