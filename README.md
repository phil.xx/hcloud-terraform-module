[[_TOC_]]

# hcloud-terraform-module

Terraform module for provisioning Servers on HCLOUD.

## Requirements

| Name | Version |
|------|---------|
| terraform | <= 0.14 |
| terraform | >= 0.13 |
| hcloud | 1.25.1 |

## Preparations

* Install Hetzner Cloud (hcloud) module
  * ```bash
    pip3 install hcloud
    ```
* Generate SSH Key
  * ```bash
    ssh-keygen -t ed25519 -C "EMAIL" -f ~/.ssh/user
    ```
* Set Hetzner Cloud Token 
  * ```bash
    export HCLOUD_TOKEN=TOKEN
    ```

## Modules

| Name | Source | Documentation |
|------|--------|---------|
| network | ./modules/network | [quick access](https://gitlab.com/phil.xx/hcloud-terraform-module/-/tree/master/modules/network) |
| server | ./modules/server | [quick access](https://gitlab.com/phil.xx/hcloud-terraform-module/-/tree/master/modules/server) |
| ssh | ./modules/ssh | [quick access](https://gitlab.com/phil.xx/hcloud-terraform-module/-/tree/master/modules/ssh) |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| server | Map of project names to configuration. | `map` | <pre>{<br>  "server01": {<br>    "label": "first_group",<br>    "server_type": "cx21"<br>  }<br>}</pre> | no |


### Commands
```bash
# Basic run with local state file
terraform init
# create and view an execution plan
terraform plan
# apply the changes
terraform apply
# destroy the Terraform-managed infrastructure
terraform destroy
```